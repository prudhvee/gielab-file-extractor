package com.prudhvee.java.GitLabFileExtractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitLabFileExtractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitLabFileExtractorApplication.class, args);
	}

}
